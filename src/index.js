import { fromEvent, merge, Subject, takeUntil } from "rxjs";
import WORDS_LIST from "./wordsList.json";

const letterRows = document.getElementsByClassName("letter-row");
const onKeyDown$ = fromEvent(document, "keydown");
let letterIndex = 0;
let letterRowIndex = 0;
let userAnswer = [];
const getRandomWord = () =>
  WORDS_LIST[Math.floor(Math.random() * WORDS_LIST.length)];
let rightWord = getRandomWord();
console.log(`Right word: ${rightWord}`);
let frondEndRightWord = document.getElementById("right-word")
frondEndRightWord.textContent = `${rightWord}`
let messageText = document.getElementById("message-text");
let numLettersRowsGreen = 0
let numTheLettersCompleteColor = 0
let winOrLose = document.getElementById("win-or-lose")
let restartButton = document.getElementById("restart-button")
const userWinOrLoose$ = new Subject();

const insertLetter = {
  next: (event) => {
    const pressedKey = event.key.toUpperCase();
    if(pressedKey === "BACKSPACE"){
      letterIndex--;
      let letterBox = Array.from(letterRows)[letterRowIndex].children[letterIndex];
      letterBox.textContent = ""
      userAnswer.pop()
      }
    if(letterRowIndex <= 6){
      if(letterIndex < 5){
        if(pressedKey.length === 1 && pressedKey.match(/[a-z]/i)){
        let letterBox = Array.from(letterRows)[letterRowIndex].children[letterIndex]
        letterBox.textContent = pressedKey
        letterIndex++
        userAnswer.push(pressedKey)
        }
      }else {
        letterRowIndex++
        if(letterRowIndex <= 6){
          letterIndex = 0
          if(pressedKey.length === 1 && pressedKey.match(/[a-z]/i)){
            let letterBox = Array.from(letterRows)[letterRowIndex].children[letterIndex]
            letterBox.textContent = pressedKey
            letterIndex++
            userAnswer.push(pressedKey)
          }
        }
        
      }
    }else {
      console.log('Todas las casillas estan llenas')
    }
    console.log(userAnswer)
  },
};

const checkWord = {
  next: (event) => {
    if (event.key === "Enter") {
      // Si la respuesta del usuario es igual a la palabra correcta:
      if(userAnswer.length !== 5){
        messageText.textContent = "Te faltan algunas letras"
        return
      }
      const rightWordArray = Array.from(rightWord)
      let isUserLetterCurrent = false
      
      for(let i = 0; i<5; i++){
        let letterColor = ""
        let letterBox = Array.from(letterRows)[letterRowIndex - 1].children[i]
        let letterPosition = Array.from(rightWord).indexOf(userAnswer[i])
        if(letterPosition === -1){
          letterColor = "letter-grey"
          numTheLettersCompleteColor++
        }else{
          if(rightWordArray[i] === userAnswer[i]){
            letterColor = "letter-green"
            isUserLetterCurrent = true
            numLettersRowsGreen++
            numTheLettersCompleteColor++
          }else {
            letterColor = "letter-yellow"
            isUserLetterCurrent = true
            numTheLettersCompleteColor++
          }
        }
        letterBox.classList.add(letterColor)
      }
      
      if(isUserLetterCurrent === true){
        rightWord = getRandomWord()
        frondEndRightWord.textContent = `${rightWord}`
        userAnswer = []
        
      }
      if(numTheLettersCompleteColor === 30){
        userWinOrLoose$.next(numLettersRowsGreen)
        restartButton.disabled = false
      }
    }
  },
};

const onWindowLoad$ = fromEvent(window, "load")
const onRestartClick$ = fromEvent(restartButton, "click")
const restartGame$ = merge(onWindowLoad$, onRestartClick$)
restartGame$.subscribe(() => {
  Array.from(letterRows).map((row)=>
  Array.from(row.children).map((letterBox) => {
    letterBox.textContent = ""
    letterBox.classList = "letter"
  }))

  letterRowIndex = 0
  letterIndex = 0
  userAnswer = []
  messageText.textContent = ""
  winOrLose.textContent = ""
  restartButton.disabled = true
  onKeyDown$.pipe(takeUntil(userWinOrLoose$)).subscribe(insertLetter);
  onKeyDown$.pipe(takeUntil(userWinOrLoose$)).subscribe(checkWord);
})

userWinOrLoose$.subscribe((item) =>{
  if(item === 30){
    winOrLose.textContent = "Win"
  }else{
    winOrLose.textContent = "Lose"
  }
})